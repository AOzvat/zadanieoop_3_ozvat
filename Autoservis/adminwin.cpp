﻿#include "adminwin.h"
#include <QFile>
#include <QTextStream>
#include <QComboBox>

adminwin::adminwin(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);

	QFile zakaznik("Pouzivatelia.txt");

	if (zakaznik.open(QFile::ReadOnly)){
		QTextStream pouzivatel (&zakaznik);
		while (!pouzivatel.atEnd()){
			QString line = pouzivatel.readLine();
			ui.comboBox->addItem(line);
		}
		zakaznik.close();
	}
}

adminwin::~adminwin() {

}

bool adminwin::ok() {
	QString heslo = ui.lineEdit_2->text();
	QMessageBox mbox;
	bool ok = true;

	if (heslo_admin!=heslo){
		ok = false;
		mbox.setText("Zle heslo!");
		mbox.exec();
		return ok;
	}

	return ok;
}

void adminwin::vymaz() {
	//vymaze oznaceneho pouzivatela v comboBoxe (v subore pouzivatelia.txt a hesla.txt)
	if (ok()) {
		QString koho = ui.comboBox->currentText();
		int index = ui.comboBox->currentIndex();

		//vymaze s pouzivatelia.txt
		QFile pomocny("Pomocny.txt");
		pomocny.open(QFile::WriteOnly | QFile::Text);
		QTextStream pomoc(&pomocny);

		QFile klient_meno("Pouzivatelia.txt");
		if (klient_meno.open(QFile::ReadOnly)) {
			QTextStream km(&klient_meno);
			while (!km.atEnd()) {
				QString line = km.readLine();
				if (line != koho) {
					pomoc << line << endl;
				}
			}
		}
		klient_meno.remove();

		pomocny.close();
		pomocny.rename("Pomocny.txt", "Pouzivatelia.txt");

		//vymaze s hesla.txt
		int n = 0;
		QFile pomocna("Pomocna.txt");
		pomocna.open(QFile::WriteOnly | QFile::Text);
		QTextStream help(&pomocna);

		QFile klient_heslo("Hesla.txt");
		if (klient_heslo.open(QFile::ReadOnly)) {
			QTextStream kh(&klient_heslo);
			while (!kh.atEnd()) {
				QString line = kh.readLine();
				n = n + 1;
				if (n != index) {
					help << line << endl;
				}
			}
		}
		klient_heslo.remove();

		pomocna.close();
		pomocna.rename("Pomocna.txt", "Hesla.txt");

		QMessageBox oznam;
		oznam.setText("Pouzivatel uspesne vymazany.");
		oznam.exec();
		close();
	}
}

void adminwin::pridaj(){
	//prida pouzivatela do suboru pouzivatelia.txt a hesla.txt
	if (ok()) {
		QString meno = ui.lineEdit_3->text();
		QString pohlavie = ui.lineEdit_4->text();
		QString heslo = ui.lineEdit_6->text();

		QFile klient_meno("Pouzivatelia.txt");
		if (klient_meno.open(QFile::Append | QFile::Text)) {
			QTextStream km(&klient_meno);
			km << endl << meno;
			klient_meno.close();
		}

		QFile klient_heslo("Hesla.txt");
		if (klient_heslo.open(QFile::Append | QFile::Text)) {
			QTextStream kh(&klient_heslo);
			kh << endl << heslo;
			klient_heslo.close();
		}

		QMessageBox oznam;
		oznam.setText("Novy pouzivatel uspesne pridany.");
		oznam.exec();
		close();
	}
}
