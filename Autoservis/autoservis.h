#ifndef AUTOSERVIS_H
#define AUTOSERVIS_H

#include <QtWidgets/QMainWindow>
#include "ui_autoservis.h"
#include <QMessageBox>
#include <loginwin.h>
#include <QFile>
#include <QTextStream>
#include <adminwin.h>
#include <QLineEdit>


class Autoservis : public QMainWindow
{
	Q_OBJECT

public:
	Autoservis(QWidget *parent = 0);
	~Autoservis();

	private slots:
		void prihlasenie();
		void zakroky();
		void auta();
		void vyber_auta();
		void vyber_zakrokov();
		void objednaj();
		void zalohovanie();
		void pridaj_auto();
		void odskrtnutie_checkBox();
		void odhlasenie();
		void admin();

private:
	Ui::AutoservisClass ui;

	QList<QString> heslo;
	bool ok;
	int index;
	QString meno;
	int n;
	QString vybrane_auto;
	QList<QString> vybrane_zakroky;
	int m;
	QList<QString> cena;
	QString celkom;
};

#endif // AUTOSERVIS_H
