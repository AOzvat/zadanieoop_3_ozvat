﻿#include "loginwin.h"
#include <QFile>
#include <QTextStream>
#include <QComboBox>

using namespace std;

loginwin::loginwin(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);

	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
	
	QFile zakaznik("Pouzivatelia.txt");

	if (zakaznik.open(QFile::ReadOnly)){
		QTextStream pouzivatel (&zakaznik);
		while (!pouzivatel.atEnd()){
			QString line = pouzivatel.readLine();
			ui.comboBox->addItem(line);
		}
		zakaznik.close();
	}
}

loginwin::~loginwin() {
	
}

int loginwin::DajMeno() {
	return meno;
}

QString loginwin::DajMeno_string() {
	return meno_string;
}

QString loginwin::DajHeslo() {
	return heslo;
}

void loginwin::ok_click() {
	meno = ui.comboBox->currentIndex();
	meno_string = ui.comboBox->currentText();
	heslo = ui.lineEdit_2->text();
	close();
}