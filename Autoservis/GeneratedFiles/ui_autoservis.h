/********************************************************************************
** Form generated from reading UI file 'autoservis.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTOSERVIS_H
#define UI_AUTOSERVIS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AutoservisClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QLabel *label;
    QGroupBox *groupBox;
    QGroupBox *groupBox_3;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_7;
    QCheckBox *checkBox_8;
    QCheckBox *checkBox_9;
    QCheckBox *checkBox_10;
    QCheckBox *checkBox_11;
    QCheckBox *checkBox_12;
    QCheckBox *checkBox_13;
    QComboBox *comboBox;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *AutoservisClass)
    {
        if (AutoservisClass->objectName().isEmpty())
            AutoservisClass->setObjectName(QStringLiteral("AutoservisClass"));
        AutoservisClass->resize(612, 548);
        centralWidget = new QWidget(AutoservisClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(460, 10, 91, 31));
        QFont font;
        font.setPointSize(12);
        pushButton->setFont(font);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(180, 20, 201, 51));
        QFont font1;
        font1.setFamily(QStringLiteral("Aharoni"));
        font1.setPointSize(30);
        label->setFont(font1);
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 80, 571, 351));
        QFont font2;
        font2.setPointSize(13);
        groupBox->setFont(font2);
        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(290, 30, 261, 301));
        groupBox_3->setFont(font2);
        checkBox = new QCheckBox(groupBox_3);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(30, 30, 211, 17));
        QFont font3;
        font3.setPointSize(10);
        checkBox->setFont(font3);
        checkBox_2 = new QCheckBox(groupBox_3);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(30, 50, 211, 17));
        checkBox_2->setFont(font3);
        checkBox_3 = new QCheckBox(groupBox_3);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(30, 70, 211, 17));
        checkBox_3->setFont(font3);
        checkBox_4 = new QCheckBox(groupBox_3);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(30, 90, 211, 17));
        checkBox_4->setFont(font3);
        checkBox_5 = new QCheckBox(groupBox_3);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setGeometry(QRect(30, 110, 211, 17));
        checkBox_5->setFont(font3);
        checkBox_6 = new QCheckBox(groupBox_3);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setGeometry(QRect(30, 130, 211, 17));
        checkBox_6->setFont(font3);
        checkBox_7 = new QCheckBox(groupBox_3);
        checkBox_7->setObjectName(QStringLiteral("checkBox_7"));
        checkBox_7->setGeometry(QRect(30, 150, 211, 17));
        checkBox_7->setFont(font3);
        checkBox_8 = new QCheckBox(groupBox_3);
        checkBox_8->setObjectName(QStringLiteral("checkBox_8"));
        checkBox_8->setGeometry(QRect(30, 170, 211, 17));
        checkBox_8->setFont(font3);
        checkBox_9 = new QCheckBox(groupBox_3);
        checkBox_9->setObjectName(QStringLiteral("checkBox_9"));
        checkBox_9->setGeometry(QRect(30, 190, 211, 17));
        checkBox_9->setFont(font3);
        checkBox_10 = new QCheckBox(groupBox_3);
        checkBox_10->setObjectName(QStringLiteral("checkBox_10"));
        checkBox_10->setGeometry(QRect(30, 210, 211, 17));
        checkBox_10->setFont(font3);
        checkBox_11 = new QCheckBox(groupBox_3);
        checkBox_11->setObjectName(QStringLiteral("checkBox_11"));
        checkBox_11->setGeometry(QRect(30, 230, 211, 17));
        checkBox_11->setFont(font3);
        checkBox_12 = new QCheckBox(groupBox_3);
        checkBox_12->setObjectName(QStringLiteral("checkBox_12"));
        checkBox_12->setGeometry(QRect(30, 250, 211, 17));
        checkBox_12->setFont(font3);
        checkBox_13 = new QCheckBox(groupBox_3);
        checkBox_13->setObjectName(QStringLiteral("checkBox_13"));
        checkBox_13->setGeometry(QRect(30, 270, 211, 17));
        checkBox_13->setFont(font3);
        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(50, 160, 201, 31));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 120, 111, 31));
        label_2->setFont(font2);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 230, 251, 31));
        QFont font4;
        font4.setPointSize(11);
        label_3->setFont(font4);
        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(40, 270, 191, 31));
        lineEdit->setFont(font4);
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(80, 70, 111, 31));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 40, 251, 31));
        label_5->setFont(font4);
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(460, 40, 91, 31));
        pushButton_2->setFont(font3);
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(90, 450, 121, 41));
        QFont font5;
        font5.setPointSize(14);
        pushButton_3->setFont(font5);
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(470, 450, 91, 31));
        pushButton_4->setFont(font3);
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(330, 450, 91, 31));
        pushButton_5->setFont(font3);
        AutoservisClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(AutoservisClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 612, 21));
        AutoservisClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(AutoservisClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        AutoservisClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(AutoservisClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        AutoservisClass->setStatusBar(statusBar);

        retranslateUi(AutoservisClass);
        QObject::connect(pushButton, SIGNAL(clicked()), AutoservisClass, SLOT(prihlasenie()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), AutoservisClass, SLOT(admin()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), AutoservisClass, SLOT(objednaj()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), AutoservisClass, SLOT(pridaj_auto()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), AutoservisClass, SLOT(odhlasenie()));

        QMetaObject::connectSlotsByName(AutoservisClass);
    } // setupUi

    void retranslateUi(QMainWindow *AutoservisClass)
    {
        AutoservisClass->setWindowTitle(QApplication::translate("AutoservisClass", "Autoservis", 0));
        pushButton->setText(QApplication::translate("AutoservisClass", "Prihlasenie", 0));
        label->setText(QApplication::translate("AutoservisClass", "Autoservis", 0));
        groupBox->setTitle(QApplication::translate("AutoservisClass", "Nech sa paci!", 0));
        groupBox_3->setTitle(QApplication::translate("AutoservisClass", "Zakroky:", 0));
        checkBox->setText(QString());
        checkBox_2->setText(QString());
        checkBox_3->setText(QString());
        checkBox_4->setText(QString());
        checkBox_5->setText(QString());
        checkBox_6->setText(QString());
        checkBox_7->setText(QString());
        checkBox_8->setText(QString());
        checkBox_9->setText(QString());
        checkBox_10->setText(QString());
        checkBox_11->setText(QString());
        checkBox_12->setText(QString());
        checkBox_13->setText(QString());
        label_2->setText(QApplication::translate("AutoservisClass", "Zoznam aut:", 0));
        label_3->setText(QApplication::translate("AutoservisClass", "Auto, ktore sa ma pridat:", 0));
        label_4->setText(QApplication::translate("AutoservisClass", "Prihlaste sa!", 0));
        label_5->setText(QApplication::translate("AutoservisClass", "Prihlaseny/a:", 0));
        pushButton_2->setText(QApplication::translate("AutoservisClass", "Admin", 0));
        pushButton_3->setText(QApplication::translate("AutoservisClass", "Objednavka", 0));
        pushButton_4->setText(QApplication::translate("AutoservisClass", "Odhlasenie", 0));
        pushButton_5->setText(QApplication::translate("AutoservisClass", "Pridat auto", 0));
    } // retranslateUi

};

namespace Ui {
    class AutoservisClass: public Ui_AutoservisClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTOSERVIS_H
