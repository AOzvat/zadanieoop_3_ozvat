/********************************************************************************
** Form generated from reading UI file 'adminwin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINWIN_H
#define UI_ADMINWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_adminwin
{
public:
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QComboBox *comboBox;
    QLabel *label_3;
    QPushButton *pushButton;
    QLabel *label_4;
    QLabel *label_5;
    QLineEdit *lineEdit_3;
    QLabel *label_6;
    QLineEdit *lineEdit_4;
    QLabel *label_8;
    QLineEdit *lineEdit_6;
    QPushButton *pushButton_2;

    void setupUi(QWidget *adminwin)
    {
        if (adminwin->objectName().isEmpty())
            adminwin->setObjectName(QStringLiteral("adminwin"));
        adminwin->resize(451, 261);
        label_2 = new QLabel(adminwin);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 20, 51, 31));
        QFont font;
        font.setPointSize(12);
        label_2->setFont(font);
        lineEdit_2 = new QLineEdit(adminwin);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(70, 20, 131, 31));
        lineEdit_2->setFont(font);
        comboBox = new QComboBox(adminwin);
        comboBox->insertItems(0, QStringList()
         << QStringLiteral("")
        );
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(50, 110, 131, 31));
        label_3 = new QLabel(adminwin);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(40, 70, 151, 31));
        label_3->setFont(font);
        pushButton = new QPushButton(adminwin);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(70, 160, 75, 31));
        label_4 = new QLabel(adminwin);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(230, 70, 151, 31));
        label_4->setFont(font);
        label_5 = new QLabel(adminwin);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(240, 110, 151, 21));
        QFont font1;
        font1.setPointSize(11);
        label_5->setFont(font1);
        lineEdit_3 = new QLineEdit(adminwin);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(300, 110, 131, 21));
        lineEdit_3->setFont(font);
        label_6 = new QLabel(adminwin);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(240, 140, 151, 21));
        label_6->setFont(font1);
        lineEdit_4 = new QLineEdit(adminwin);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(300, 140, 131, 21));
        lineEdit_4->setFont(font);
        label_8 = new QLabel(adminwin);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(240, 170, 151, 21));
        label_8->setFont(font1);
        lineEdit_6 = new QLineEdit(adminwin);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(300, 170, 131, 21));
        lineEdit_6->setFont(font);
        pushButton_2 = new QPushButton(adminwin);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(300, 210, 75, 31));

        retranslateUi(adminwin);
        QObject::connect(pushButton, SIGNAL(clicked()), adminwin, SLOT(vymaz()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), adminwin, SLOT(pridaj()));

        QMetaObject::connectSlotsByName(adminwin);
    } // setupUi

    void retranslateUi(QWidget *adminwin)
    {
        adminwin->setWindowTitle(QApplication::translate("adminwin", "adminwin", 0));
        label_2->setText(QApplication::translate("adminwin", "Heslo:", 0));
        label_3->setText(QApplication::translate("adminwin", "Vymazanie klienta:", 0));
        pushButton->setText(QApplication::translate("adminwin", "Vymazat", 0));
        label_4->setText(QApplication::translate("adminwin", "Pridat klienta:", 0));
        label_5->setText(QApplication::translate("adminwin", "Meno:", 0));
        label_6->setText(QApplication::translate("adminwin", "Pohlavie:", 0));
        label_8->setText(QApplication::translate("adminwin", "Heslo:", 0));
        pushButton_2->setText(QApplication::translate("adminwin", "Pridat", 0));
    } // retranslateUi

};

namespace Ui {
    class adminwin: public Ui_adminwin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINWIN_H
