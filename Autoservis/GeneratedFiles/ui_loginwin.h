/********************************************************************************
** Form generated from reading UI file 'loginwin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWIN_H
#define UI_LOGINWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_loginwin
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton;
    QComboBox *comboBox;

    void setupUi(QDialog *loginwin)
    {
        if (loginwin->objectName().isEmpty())
            loginwin->setObjectName(QStringLiteral("loginwin"));
        loginwin->resize(244, 158);
        QFont font;
        font.setPointSize(10);
        loginwin->setFont(font);
        label = new QLabel(loginwin);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 51, 31));
        QFont font1;
        font1.setPointSize(12);
        label->setFont(font1);
        label_2 = new QLabel(loginwin);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 50, 51, 31));
        label_2->setFont(font1);
        lineEdit_2 = new QLineEdit(loginwin);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(90, 50, 131, 31));
        lineEdit_2->setFont(font1);
        pushButton = new QPushButton(loginwin);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(90, 100, 75, 31));
        comboBox = new QComboBox(loginwin);
        comboBox->insertItems(0, QStringList()
         << QStringLiteral("")
        );
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(90, 10, 131, 31));

        retranslateUi(loginwin);
        QObject::connect(pushButton, SIGNAL(clicked()), loginwin, SLOT(ok_click()));

        QMetaObject::connectSlotsByName(loginwin);
    } // setupUi

    void retranslateUi(QDialog *loginwin)
    {
        loginwin->setWindowTitle(QApplication::translate("loginwin", "loginwin", 0));
        label->setText(QApplication::translate("loginwin", "Meno:", 0));
        label_2->setText(QApplication::translate("loginwin", "Heslo:", 0));
        pushButton->setText(QApplication::translate("loginwin", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class loginwin: public Ui_loginwin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWIN_H
