﻿#pragma once
#include <QDialog>
#include "ui_adminwin.h"
#include <QMessageBox>

class adminwin : public QDialog {
	Q_OBJECT

public:
	adminwin(QWidget * parent = Q_NULLPTR);
	~adminwin();

	public slots:
		void vymaz();
		void pridaj();
		bool ok();

private:
	Ui::adminwin ui;
	QString heslo_admin = "1234";
};
