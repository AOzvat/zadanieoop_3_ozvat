﻿#pragma once
#ifndef LOGINWIN_H
#define LOGINWIN_H
#include <QDialog>
#include "ui_loginwin.h"
#include <QComboBox>

class loginwin : public QDialog {
	Q_OBJECT

public:
	loginwin(QWidget * parent = Q_NULLPTR);
	~loginwin();

	int DajMeno();
	QString DajMeno_string();
	QString DajHeslo();

	public slots:
		void ok_click();

private:
	Ui::loginwin ui;
	int meno;
	QString meno_string;
	QString heslo;
};

#endif // LOGINWIN_H