#include "autoservis.h"

Autoservis::Autoservis(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

Autoservis::~Autoservis()
{

}

void Autoservis::prihlasenie() {
	loginwin login;
	QMessageBox mbox;
	login.exec();

	QFile hesla("Hesla.txt");
	if (hesla.open(QFile::ReadOnly)) {
		QTextStream sifri(&hesla);
		while (!sifri.atEnd()) {
			QString sifra = sifri.readLine();
			heslo.push_back(sifra);
		}
		hesla.close();
	}

	index = login.DajMeno()-1;
	meno = login.DajMeno_string();

	if (login.DajHeslo() != heslo[index]) {
		ok = false;
	}
	else {
		ok = true;
	}

	if (ok == true) {
		mbox.setText("Prihlasenie uspesne!");
		mbox.exec();
		ui.label_4->setText(meno);
		zakroky();
		auta();
		odskrtnutie_checkBox();
	}
	else {
		mbox.setText("Zle heslo!");
		mbox.exec();
	}
}

void Autoservis::zakroky() {
	//nacitaju sa so suboru nazvy zakrokov do CheckBoxov
	QFile zakrok("zakroky.txt");

	if (zakrok.open(QFile::ReadOnly)) {
		QTextStream praca (&zakrok);

		QString riadok = praca.readLine();
		ui.checkBox->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_2->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_3->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_4->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_5->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_6->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_7->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_8->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_9->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_10->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_11->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_12->setText(riadok);
		riadok = praca.readLine();
		ui.checkBox_13->setText(riadok);

		zakrok.close();
	}
}

void Autoservis::auta() {
	//nacitaju sa so suboru auta do ComboBoxu
	n = 0;

	QFile automobil(meno + ".txt");

	if (automobil.open(QFile::ReadOnly)) {
		QTextStream car(&automobil);
		while (!car.atEnd()) {
			QString riadok = car.readLine();
			ui.comboBox->addItem(riadok);
			n = n + 1;
		}
		automobil.close();
	}
}

void Autoservis::vyber_auta() {
	//uzivatel si vyberie auto so svojho zoznamu
	for (int i = 0; i < n; i++) {
		if (ui.comboBox->currentIndex() == i) {
			vybrane_auto = ui.comboBox->currentText();
		}
	}
}

void Autoservis::vyber_zakrokov() {
	//pouzivatel odskrtne checkBox a vyberie si zakrok
	m = 0;

	QList<QString> prices;
	QFile ceni("Cena.txt");
	if (ceni.open(QFile::ReadOnly)) {
		QTextStream cene(&ceni);
		while (!cene.atEnd()) {
			QString price = cene.readLine();
			prices.push_back(price);
		}
		ceni.close();
	}

	if (ui.checkBox->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox->text());
		m = m + 1;
		cena.push_back(prices[0]);
	}
	if (ui.checkBox_2->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_2->text());
		m = m + 1;
		cena.push_back(prices[1]);
	}
	if (ui.checkBox_3->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_3->text());
		m = m + 1;
		cena.push_back(prices[2]);
	}
	if (ui.checkBox_4->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_4->text());
		m = m + 1;
		cena.push_back(prices[3]);
	}
	if (ui.checkBox_5->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_5->text());
		m = m + 1;
		cena.push_back(prices[4]);
	}
	if (ui.checkBox_6->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_6->text());
		m = m + 1;
		cena.push_back(prices[5]);
	}
	if (ui.checkBox_7->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_7->text());
		m = m + 1;
		cena.push_back(prices[6]);
	}
	if (ui.checkBox_8->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_8->text());
		m = m + 1;
		cena.push_back(prices[7]);
	}
	if (ui.checkBox_9->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_9->text());
		m = m + 1;
		cena.push_back(prices[8]);
	}
	if (ui.checkBox_10->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_10->text());
		m = m + 1;
		cena.push_back(prices[9]);
	}
	if (ui.checkBox_11->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_11->text());
		m = m + 1;
		cena.push_back(prices[10]);
	}
	if (ui.checkBox_12->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_12->text());
		m = m + 1;
		cena.push_back(prices[11]);
	}
	if (ui.checkBox_13->isChecked()) {
		vybrane_zakroky.push_back(ui.checkBox_13->text());
		m = m + 1;
		cena.push_back(prices[12]);
	}
}

void Autoservis::objednaj() {
	//vytvorenie faktury
	if (ok == true) {
		vyber_auta();
		vyber_zakrokov();

		QFile objednavki("Faktura " + vybrane_auto + ".txt");
		objednavki.open(QFile::WriteOnly | QFile::Text);
		QTextStream objednavka(&objednavki);

		objednavka << "Dodavatel:		Autoservis" << endl << endl;
		objednavka << "Odberatel:		" + meno << endl << endl;
		objednavka << "Zvolany automobil:	" + vybrane_auto << endl << endl << endl;
		for (int i = 0; i < m; i++) {
			objednavka << vybrane_zakroky[i] << "			" << cena[i] << endl;
		}

		int celkovo = 0;
		for (int i = 0; i < m; i++) {
			celkovo = celkovo + cena[i].toInt();
		}
		celkom = QString::number(celkovo);
		objednavka << endl << "CELKOM EUR:		" + celkom;

		objednavki.close();

		QMessageBox oznam;
		oznam.setText("Objednavka vykonana.");
		oznam.exec();

		//zalohovanie objednavok
		zalohovanie();
		odskrtnutie_checkBox();
		vybrane_zakroky.clear();
		cena.clear();
	}
	else{
		QMessageBox oznam;
		oznam.setText("Objednavku nie je moze vykonat, nie ste prihlaseny.");
		oznam.exec();
		odskrtnutie_checkBox();
	}
}

void Autoservis::zalohovanie() {
	QFile zalohi(meno + " " + vybrane_auto + ".txt");
	QString ulozit;
	QString suma;

	if (zalohi.open(QFile::ReadOnly)) {
		QTextStream zalohovat(&zalohi);
		while (!zalohovat.atEnd()) {
			ulozit = zalohovat.readAll();
		}
		zalohi.close();
	}

	if (ulozit.length() != 0) {
		QStringList split = ulozit.split('~');
		int suma_int = split[split.length() - 2].toInt() + celkom.toInt();
		suma = QString::number(suma_int);
	}
	else {
		suma = celkom;
	}

	if (zalohi.open(QFile::Append | QFile::Text)) {
		QTextStream zaloha(&zalohi);

		for (int i = 0; i < m; i++) {
			zaloha << vybrane_zakroky[i] << "			" << cena[i] << endl;
		}
		zaloha << endl << "CELKOM EUR:		" + celkom << endl;
		zaloha << "Spolu:		" << "~" << suma << "~" << endl;
		zaloha << "------------------------------------------" << endl;
		zalohi.close();
	}
}

void Autoservis::pridaj_auto() {
	//zapis automobilu do zoznamu
	if (ok == true) {
		QString text = ui.lineEdit->text();

		QFile automobil(meno + ".txt");
		if (automobil.open(QFile::Append | QFile::Text)) {
			QTextStream car(&automobil);
			car << endl << text;
			automobil.close();

			QMessageBox oznam;
			oznam.setText("Pridanie auta uspesne.");
			oznam.exec();
			ui.lineEdit->clear();
			ui.comboBox->clear();
			auta();
		}
	}
	else {
		QMessageBox oznam;
		oznam.setText("Pridat auto nie je moze, nie ste prihlaseny.");
		oznam.exec();
		odskrtnutie_checkBox();
	}
}

void Autoservis::odskrtnutie_checkBox() {
	ui.checkBox->setChecked(false);
	ui.checkBox_2->setChecked(false);
	ui.checkBox_3->setChecked(false);
	ui.checkBox_4->setChecked(false);
	ui.checkBox_5->setChecked(false);
	ui.checkBox_6->setChecked(false);
	ui.checkBox_7->setChecked(false);
	ui.checkBox_8->setChecked(false);
	ui.checkBox_9->setChecked(false);
	ui.checkBox_10->setChecked(false);
	ui.checkBox_11->setChecked(false);
	ui.checkBox_12->setChecked(false);
	ui.checkBox_13->setChecked(false);

	ui.lineEdit->clear();
}

void Autoservis::odhlasenie() {
	if (ok == true) {
		ok = false;
		index = -1;
		meno = "";
		n = 0;
		m = 0;
		vybrane_auto = "";
		vybrane_zakroky.clear();
		cena.clear();
		celkom = "";

		ui.lineEdit->clear();
		ui.comboBox->clear();
		ui.checkBox->setText("");
		ui.checkBox_2->setText("");
		ui.checkBox_3->setText("");
		ui.checkBox_4->setText("");
		ui.checkBox_5->setText("");
		ui.checkBox_6->setText("");
		ui.checkBox_7->setText("");
		ui.checkBox_8->setText("");
		ui.checkBox_9->setText("");
		ui.checkBox_10->setText("");
		ui.checkBox_11->setText("");
		ui.checkBox_12->setText("");
		ui.checkBox_13->setText("");
		odskrtnutie_checkBox();

		QMessageBox oznam;
		oznam.setText("Odhlasenie uspesne!");
		oznam.exec();
		odskrtnutie_checkBox();
	}
	else {
		QMessageBox oznam;
		oznam.setText("Odhlasenie nie je mozne, nikto nebol prihlaseny.");
		oznam.exec();
		odskrtnutie_checkBox();
	}
}

void Autoservis::admin() {
	if (ok == true) {
		odhlasenie();
	}
	else {
		odskrtnutie_checkBox();
	}
	adminwin admin;
	admin.exec();
}

